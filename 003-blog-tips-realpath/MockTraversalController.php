<?php

namespace MyNamespace\MyExtension\Controller\Index;

use Magento\Framework\App\Filesystem\DirectoryList;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_dir;
    protected $_request;
    protected $_directoryResolver;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\App\Filesystem\DirectoryResolver $directoryResolver
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_request = $request;
        $this->_dir = $dir;
        $this->_directoryResolver = $directoryResolver;
        return parent::__construct($context);
    }

    public function execute()
    {
        $insecurePathToLocalFile = $this->_request->getParam('local_file_path');
        $realMediaDirectory = DirectoryList::MEDIA;

        // $insecurePathToLocalFile can't be trusted, so validate that it a real path inside the public media directory

        $trustedPath = null;
        if ($this->_directoryResolver->validatePath($insecurePathToLocalFile, $realMediaDirectory)) {
            $trustedPath = $insecurePathToLocalFile;
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Potential traversal attack detected. Directory %1 is not under media folder', $insecurePathToLocalFile)
            );
        }

        // ...

        // Now we can do something with $trustedPath

        // ...

        return $this->_pageFactory->create();
    }
}
